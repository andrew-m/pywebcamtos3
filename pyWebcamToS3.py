import datetime, subprocess
filenameRoot = "webcam"
filenameSuffix = ".jpg"
dateComponent = datetime.datetime.now().strftime("-%Y%m%d-%H%M%S")
filename = filenameRoot + dateComponent + filenameSuffix
subprocess.call(["fswebcam", "--verbose -c ./fswebcam.config " + filename])
